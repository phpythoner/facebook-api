  var appid = '211536099005797';
  window.fbAsyncInit = function() {
    FB.init({
      appId  : appid,
      status : true,
      cookie : true,
      xfbml  : true
  });
};

/**
 * facebook open graph like event
 * @author Channing Channing@r2games.com
 * @version 1.0
 * @return void
 */
var openLike = function() {
    // like
    //  FB.api(
    //     'me/og.likes',
    //     'post',
    //     {object:"http://dragon.r2games.com/news/view/?id=4906"},
    //     function() {
    //         console.log(response);
    //     }
    // );
    FB.api(
      "/me/objects/object",
      "POST",
      {
          "og:type": "object",
          "og:image": "http://r2cdn.r2games.com/en/dp/images/bg_01.jpg",
          "og:title": "Dragon Pals Official Site - Free Fantasy Turn Based MMORPG, Free to Play!",
          "fb:app_id": appid,
          "og:url": "http://dragon.r2games.com/"
      },
      function (response) {
        console.log(response);
        if (response && !response.error) {
          /* handle the result */
        }
      }
  );
}